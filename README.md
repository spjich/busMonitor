#运行环境
java1.7/java1.6

tomcat7

redis可选



#运行首页
![TMD](PIC/1.jpg)
#step1.设置模拟线路
在输入框中输入你要模拟的线路,本程序就会模拟一辆bus在该线路上的移动
#step2.查看线路路径 
这个功能可以查看到在step1中设置的线路的路径情况
![TMD](PIC/2.jpg)
#step3.模拟车辆移动
可以观察车辆从起点->终点的移动（刷新周期10s），因为本工程只是一个工具，所以目前只支持单反向
![TMD](PIC/3.jpg)
#step4.模拟重置 
将车辆移动到原点，重新跑线路

Tips:
由于业务需要，本工程使用了redis，如果使用者无需redis，可以配置configure.properties里的is_redis_enable字段
公交位置系统的实现可以参考
http://spjich.iteye.com/blog/2264025