package com.ji.model;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 
 * @Title:路径实体
 * @Description:
 * @Author:吉
 * @Since:2015年12月22日
 * @Version:1.1.0
 */
public class PathBean {
	private double lat;

	private double lng;

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

    @Override
    public String toString()
    {
        return ToStringBuilder.reflectionToString(this);
    }
}
