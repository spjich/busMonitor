package com.ji.model;

/**
 * 
 * @Title:页面展现对象
 * @Description:
 * @Author:吉
 * @Since:2015年12月22日
 * @Version:1.1.0
 */
public class Point
{

    private String icon;

    private String position;

    public String getIcon()
    {
        return icon;
    }

    public void setIcon(String icon)
    {
        this.icon = icon;
    }

    public String getPosition()
    {
        return position;
    }

    public void setPosition(String position)
    {
        this.position = position;
    }

    public Point(PathBean bean)
    {
        this.icon = "'http://webapi.amap.com/theme/v1.3/markers/n/mark_b.png'";
        this.position = "[" + bean.getLng() + "," + bean.getLat() + "]";
    }
}
