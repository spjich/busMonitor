package com.ji.onstart;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.ji.thread.BusMoveThread;

/**
 * 
 * @Title:tomcat启动监听器
 * @Description:
 * @Author:吉
 * @Since:2015年12月22日
 * @Version:1.1.0
 */
@WebListener
public class StartListener implements ServletContextListener
{

    @Override
    public void contextDestroyed(ServletContextEvent arg0)
    {

    }

    @Override
    public void contextInitialized(ServletContextEvent arg0)
    {
        BusMoveThread ins = BusMoveThread.getInst();
        ScheduledExecutorService scheduledThreadPool = Executors.newScheduledThreadPool(1);
        scheduledThreadPool.scheduleAtFixedRate(ins, 10, 10, TimeUnit.SECONDS);
    }

}
