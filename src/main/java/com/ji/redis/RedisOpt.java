package com.ji.redis;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 
 * @Title:redis操作基础类
 * @Description:
 * @Author:吉
 * @Since:2015年12月22日
 * @Version:1.1.0
 */
@SuppressWarnings("unchecked")
public interface RedisOpt {
	/**
	 * 删除某个key?
	 * 
	 * @param redisKey
	 *
	 */
	public void delKey(String redisKey);

	/**
	 * 是否存在对应的key
	 * 
	 * @param redisKey
	 * @return
	 *
	 */
	public boolean hasKey(String redisKey);

	/**
	 * 添加
	 * 
	 * @param <T>
	 * 
	 * @param redisKey
	 * @param values
	 *
	 */

	public <T> void put(String redisKey, T values);

	/**
	 * 获得
	 * 
	 * @param redisKey
	 * @return
	 *
	 */
	public <T> T get(String redisKey);

	//////////////////////////////////////////// hash///////////////////////////////////////////////

	/**
	 * 插入list数据
	 * 
	 * @param <T>
	 * 
	 * @param redisKey
	 * @param values
	 *
	 */
	public <T> void pusList(String redisKey, T... values);

	/**
	 * 获得list数据
	 * 
	 * @param <T>
	 * 
	 * @param redisKey
	 * @param start
	 * @param end
	 * @return
	 *
	 */
	public <T> List<T> getList(String redisKey, int start, int end);

	/**
	 * 获得list里所有数�?
	 * 
	 * @param <T>
	 * 
	 * @param redisKey
	 * @return
	 *
	 */
	public <T> List<T> getListAll(String redisKey);

	//////////////////////////////////////////// set///////////////////////////////////////////////
	/**
	 * 插入set数据
	 * 
	 * @param <T>
	 * 
	 * @param redisKey
	 * @param values
	 *
	 */
	public <T> void putSet(String redisKey, T... values);

	/**
	 * 获得集合数据
	 * 
	 * @param <T>
	 * 
	 * @param redisKey
	 * @return
	 *
	 */
	public <T> Set<T> getSet(String redisKey);

	//////////////////////////////////////////// map///////////////////////////////////////////////
	/**
	 * 插入map类型数据
	 * 
	 * @param <T>
	 * 
	 * @param redisKey
	 * @param map
	 *
	 */
	public <T> void putMap(String redisKey, Map<String, T> map);

	/**
	 * 获得map�?
	 * 
	 * @param redisKey
	 * @param map
	 * @return
	 *
	 */
	public Map<Object, Object> getMapAll(String redisKey);

	/**
	 * 从map中获得对应对�?
	 * 
	 * @param <T>
	 * 
	 * @param redisKey
	 * @return
	 *
	 */
	public <T> T getByMapKey(String redisKey, String mapKey);
}
