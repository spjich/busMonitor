package com.ji.redis;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.data.redis.core.StringRedisTemplate;

/**
 * 
 * @Title:redisString操作类
 * @Description:
 * @Author:吉
 * @Since:2015年12月22日
 * @Version:1.1.0
 */
@SuppressWarnings("unchecked")
public class RedisString implements RedisOpt {
	private StringRedisTemplate stringRedisTemplate;

	public StringRedisTemplate getStringRedisTemplate() {
		return stringRedisTemplate;
	}

	public void setStringRedisTemplate(StringRedisTemplate stringRedisTemplate) {
		this.stringRedisTemplate = stringRedisTemplate;
	}

	/**
	 * 删除某个�?
	 * 
	 * @param redisKey
	 *
	 */
	public void delKey(String redisKey) {
		stringRedisTemplate.delete(redisKey);
	}

	/**
	 * 是否存在对应的key
	 * 
	 * @param redisKey
	 * @return
	 *
	 */
	public boolean hasKey(String redisKey) {
		return stringRedisTemplate.hasKey(redisKey);
	}

	//////////////////////////////////////////// String///////////////////////////////////////////////
	/**
	 * 添加String类型数据
	 * 
	 * @param <T>
	 * 
	 * @param redisKey
	 * @param values
	 *
	 */
	public <T> void put(String redisKey, T values) {
		stringRedisTemplate.opsForValue().set(redisKey, (String) values);
	}

	/**
	 * 获得String类型
	 * 
	 * @param redisKey
	 * @return
	 *
	 */
	public String get(String redisKey) {
		return stringRedisTemplate.opsForValue().get(redisKey);
	}
	//////////////////////////////////////////// hash///////////////////////////////////////////////

	/**
	 * 插入list数据
	 * 
	 * @param <T>
	 * 
	 * @param redisKey
	 * @param values
	 *
	 */
	public <T> void pusList(String redisKey, T... values) {
		if (values == null || values.length == 0) {
			return;
		}
		for (T val : values) {
			stringRedisTemplate.opsForList().rightPush(redisKey, (String) val);
		}
	}

	/**
	 * 获得list数据
	 * 
	 * @param redisKey
	 * @param start
	 * @param end
	 * @return
	 *
	 */
	public List<String> getList(String redisKey, int start, int end) {
		return stringRedisTemplate.opsForList().range(redisKey, start, end);
	}

	/**
	 * 获得list里所有数�?
	 * 
	 * @param redisKey
	 * @return
	 *
	 */
	public List<String> getListAll(String redisKey) {
		return getList(redisKey, 0, -1);
	}

	//////////////////////////////////////////// set///////////////////////////////////////////////
	/**
	 * 插入set数据
	 * 
	 * @param <T>
	 * 
	 * @param redisKey
	 * @param values
	 *
	 */
	public <T> void putSet(String redisKey, T... values) {
		if (values == null || values.length == 0) {
			return;
		}
		for (T val : values) {
			stringRedisTemplate.opsForSet().add(redisKey, (String) val);
		}
	}

	/**
	 * 获得集合数据
	 * 
	 * @param redisKey
	 * @return
	 *
	 */
	public Set<String> getSet(String redisKey) {
		return stringRedisTemplate.opsForSet().members(redisKey);
	}

	//////////////////////////////////////////// map///////////////////////////////////////////////
	/**
	 * 插入map类型数据(value为对象，此方法会自动将对象转化成json)
	 * 
	 * @param <T>
	 * 
	 * @param redisKey
	 * @param map
	 *
	 */
	public <T> void putMap(String redisKey, Map<String, T> map) {
		if (map == null || map.size() == 0) {
			return;
		}
		Iterator<String> it = map.keySet().iterator();
		while (it.hasNext()) {
			String k = it.next();
			T v = map.get(k);
			stringRedisTemplate.opsForHash().put(redisKey, k, v);
		}
	}

	/**
	 * 获得map�?
	 * 
	 * @param redisKey
	 * @param map
	 * @return
	 *
	 */
	public Map<Object, Object> getMapAll(String redisKey) {
		return stringRedisTemplate.opsForHash().entries(redisKey);
	}

	/**
	 * 从map中获得对应对�?
	 * 
	 * @param redisKey
	 * @return
	 *
	 */
	public String getByMapKey(String redisKey, String mapKey) {
		return (String) stringRedisTemplate.opsForHash().get(redisKey, mapKey);
	}

}
