package com.ji.sevlet.ajax;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ji.model.PathBean;
import com.ji.thread.BusMoveThread;
import com.ji.util.AjaxUtil;

/**
 * 
 * @Title:ajax公交位置请求
 * @Description:
 * @Author:吉
 * @Since:2015年12月22日
 * @Version:1.1.0
 */
@WebServlet(name = "BusMoveSevlet", urlPatterns = "/BusMoveSevlet")
public class BusMoveSevlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    private static final Gson gson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PathBean po = BusMoveThread.getInst().getPosition();
        AjaxUtil.ajax(response, gson.toJson(po));
	}
}
