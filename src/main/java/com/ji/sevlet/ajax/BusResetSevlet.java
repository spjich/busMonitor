package com.ji.sevlet.ajax;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ji.thread.BusMoveThread;
import com.ji.util.AjaxUtil;
import com.ji.util.ParamConfig;

/**
 * 
 * @Title:公交位置重置
 * @Description:
 * @Author:吉
 * @Since:2015年12月22日
 * @Version:1.1.0
 */
@WebServlet(name = "BusResetSevlet", urlPatterns = "/BusResetSevlet")
public class BusResetSevlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        BusMoveThread.getInst().reset();
		ParamConfig.getInstance().init();
		AjaxUtil.ajax(response, "操作成功");
	}
}
