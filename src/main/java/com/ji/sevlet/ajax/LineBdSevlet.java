package com.ji.sevlet.ajax;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ji.thread.BusMoveThread;
import com.ji.util.AjaxUtil;
import com.ji.util.Const;
import com.ji.util.PathLine;

/**
 * 
 * @Title:线路绑定
 * @Description:
 * @Author:吉
 * @Since:2015年12月22日
 * @Version:1.1.0
 */
@WebServlet(name = "LineBdSevlet", urlPatterns = "/LineBdSevlet")
public class LineBdSevlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String pl = request.getParameter("lineinfo");
		String lname = request.getParameter("lname");
		if (pl == null || lname == null) {
			AjaxUtil.ajax(response, Const.FAIL);
		} else {
			PathLine.setPath(lname, pl);
			BusMoveThread.getInst().reset();
			AjaxUtil.ajax(response, Const.SUC);
		}
	}
}
