package com.ji.sevlet.ajax;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ji.model.PathBean;
import com.ji.model.Point;
import com.ji.util.AjaxUtil;
import com.ji.util.PathLine;

/**
 * 
 * @Title:路径展示
 * @Description:
 * @Author:吉
 * @Since:2015年12月22日
 * @Version:1.1.0
 */
@WebServlet(name = "MonitorSevlet", urlPatterns = "/MonitorSevlet")
public class MonitorSevlet extends HttpServlet
{

    private static final long serialVersionUID = 1L;

    private static final Gson gson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        doPost(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
		PathBean[] arr = PathLine.getPath();
        List<Point> list = new ArrayList<>();
        for (PathBean bean : arr)
        {
            list.add(new Point(bean));
        }
		// System.out.println(StringUtils.remove(gson.toJson(list.toArray()),
		// "\""));
        AjaxUtil.ajax(response, StringUtils.remove(gson.toJson(list.toArray()), "\""));
    }
}
