package com.ji.sevlet.jump;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ji.util.Const;
import com.ji.util.ParamConfig;
import com.ji.util.PathLine;

/**
 * 
 * @Title:跳转页面
 * @Description:
 * @Author:吉
 * @Since:2015年12月22日
 * @Version:1.1.0
 */
@WebServlet(name = "BusMoveJumpSevlet", urlPatterns = "/BusMoveJumpSevlet")
public class BusMoveJumpSevlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setAttribute("line", PathLine.getLname());
		request.setAttribute("mac", ParamConfig.getInstance().getString(Const.AP_MAC));
		request.getRequestDispatcher("draw_bus.jsp").forward(request, response);
	}
}
