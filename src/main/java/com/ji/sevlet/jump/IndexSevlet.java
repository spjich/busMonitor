package com.ji.sevlet.jump;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ji.util.PathLine;

/**
 * 
 * @Title:首页
 * @Description:
 * @Author:吉
 * @Since:2015年12月22日
 * @Version:1.1.0
 */
@WebServlet(name = "IndexSevlet", urlPatterns = "/IndexSevlet")
public class IndexSevlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("line", PathLine.getLname());
		request.getRequestDispatcher("index.jsp").forward(request, response);
	}
}
