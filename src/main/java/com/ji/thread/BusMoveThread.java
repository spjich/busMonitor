package com.ji.thread;

import java.util.Date;

import com.ji.model.PathBean;
import com.ji.redis.RedisString;
import com.ji.util.Const;
import com.ji.util.ParamConfig;
import com.ji.util.PathLine;
import com.ji.util.SpringHolder;

/**
 * 
 * @Title:公交移动刷新线程-单例
 * @Description:
 * @Author:吉
 * @Since:2015年12月22日
 * @Version:1.1.0
 */
public class BusMoveThread implements Runnable {

	private static BusMoveThread instance;
	/* 更新时间 */
	private Date updateTime = new Date();
	/* 路径实体 */
	private PathBean position;

	private static final String REDIS_KEY = "gpsflow";

	private BusMoveThread() {
		super();
	}

	public static synchronized BusMoveThread getInst() {
		if (instance == null) {
			instance = new BusMoveThread();
		}
		return instance;
	}

	@Override
	public void run() {
		try {
			PathBean[] arr = PathLine.getPath();
			if (arr == null) {
				return;
			}
			Date now = new Date();
			long dif = now.getTime() - updateTime.getTime();
			int index = (int) (dif / (10 * 1000)) - 1;
			if (index > arr.length) {
				index = arr.length;
			}
			System.out.println(arr[index]);
			setPosition(arr[index]);
			boolean isRedEnable = ParamConfig.getInstance().getBoolean(Const.IS_REDIS_EN);
			if (isRedEnable) {
				RedisString strRedis = SpringHolder.getApplicationContext().getBean(RedisString.class);
				String redis = "{\"apmac\":\"" + ParamConfig.getInstance().getString(Const.AP_MAC)
						+ "\",\"time\":\"15-12-03 07:14:59\",\"navist\":\"A\",\"latitude\":\"" + position.getLat()
						+ "\",\"longitude\":\"" + position.getLng() + "\",\"speed\":\"16.8\",\"direction\":\"345.3\"}";
				strRedis.pusList(REDIS_KEY, redis);
			}
		} catch (Exception e) {
			// e.printStackTrace();
		}
	}

	public PathBean getPosition() {
		return position;
	}

	public void setPosition(PathBean position) {
		this.position = position;
	}

	public void reset() {
		updateTime = new Date();
	}
}
