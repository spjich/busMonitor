package com.ji.util;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

/**
 * 
 * @Title:ajax工具
 * @Description:
 * @Author:吉
 * @Since:2015年12月22日
 * @Version:1.1.0
 */
public class AjaxUtil
{

    /**
	 * ajax返回
	 * 
	 * @param response
	 * @param content
	 *
	 */
    public static void ajax(HttpServletResponse response, String content)
    {

        PrintWriter pw = null;
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=utf-8");
        try
        {
            pw = response.getWriter();
            pw.println(content);
            pw.flush();
        }
        catch (IOException e)
        {
            e.printStackTrace();
            throw new RuntimeException("response error!");
        }
        finally
        {
            if (pw != null)
            {
                pw.close();
                pw = null;
            }
        }
    }
}
