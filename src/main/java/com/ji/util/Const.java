package com.ji.util;

/**
 * 
 * @Title:常量
 * @Description:
 * @Author:吉
 * @Since:2015年12月22日
 * @Version:1.1.0
 */
public class Const {
	/**
	 * 返回结果-成功
	 */
	public static final String SUC = "0";
	/**
	 * 返回结果-失败
	 */
	public static final String FAIL = "1";
	/**
	 * 车载gps或网卡的mac地址
	 */
	public static final String AP_MAC = "ap_mac";

	public static final String IS_REDIS_EN = "is_redis_enable";
}
