package com.ji.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

/**
 * 
 * @Title:配置文件读取类(初始化时加载到内存中)
 * @Description:配置文件读取类
 * @Author:吉
 * @Since:2015-4-24
 * @Version:1.1.0
 */
public class ParamConfig {

	private final static String configName = "configure.properties";

	private static ParamConfig instance;

	private Map<String, Object> map = new HashMap<String, Object>();

	private ParamConfig() {
		super();
	}

	/**
	 * 单例构造
	 * 
	 * @return 单例
	 * 
	 */
	public static ParamConfig getInstance() {
		if (null == instance) {
			instance = new ParamConfig();
			instance.init();
			return instance;
		} else {
			return instance;
		}
	}

	/**
	 * 初始化配置文件
	 * 
	 */
	public void init() {
		InputStream inputStream = this.getClass().getClassLoader()
				.getResourceAsStream(configName);
		Properties p = new Properties();
		try {
			p.load(inputStream);
			for (Entry<Object, Object> entry : p.entrySet()) {
				String key = (String) entry.getKey();
				String value = (String) entry.getValue();
				map.put(key, value);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (null != inputStream) {
					inputStream.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public String getString(String key) {
		return (String) map.get(key);
	}

	public long getLong(String key) {
		return Long.valueOf((String) map.get(key));
	}

	public int getInt(String key) {
		return Integer.valueOf((String) map.get(key));
	}

	public Boolean getBoolean(String key) {
		return Boolean.valueOf((String) map.get(key));
	}

	public static void main(String[] args) {
		System.out.println(ParamConfig.getInstance().getString("1"));
		System.out.println(ParamConfig.getInstance().getInt("3"));
	}
}
