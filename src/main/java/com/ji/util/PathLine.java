package com.ji.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.ji.model.PathBean;

/**
 * 
 * @Title:用户页面设置的线路路径
 * @Description:
 * @Author:吉
 * @Since:2015年12月22日
 * @Version:1.1.0
 */
public class PathLine {

	private static PathBean[] arr;
	private static String lname;
	private static final Gson gson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();

	public static PathBean[] setPath(String linename, String pStr) {
		TypeToken<PathBean[]> type = new TypeToken<PathBean[]>() {
		};
		PathBean[] beanlist = gson.fromJson(pStr, type.getType());
		arr = beanlist;
		lname = linename;
		return beanlist;
	}

	public static PathBean[] getPath() {
		return arr;
	}

	public static String getLname() {
		return lname;
	}

	public static void setLname(String lname) {
		PathLine.lname = lname;
	}

}
