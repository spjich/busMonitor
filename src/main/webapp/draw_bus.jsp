<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page isELIgnored="false"%>
<!doctype html>
<html>
<head>
    <meta charset="gb2312">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no, width=device-width">
    <title>模拟车辆移动</title>
    <link rel="stylesheet" href="http://cache.amap.com/lbs/static/main1119.css"/>
    <script type="text/javascript"
            src="http://webapi.amap.com/maps?v=1.3&key=5d9167e66df52a5615d679474dea454a&plugin=AMap.LineSearch"></script>
    <script type="text/javascript" src="http://cache.amap.com/lbs/static/addToolbar.js"></script>
    <script type="text/javascript" src="jquery-2.0.3.js"></script>
</head>
<body>
<div id="aa"></div>
<div id="content"></div>
<div id="container"></div>
<script language="javascript">
    /*
     * 该示例主要流程分为三个步骤
     * 1. 首先调用公交路线查询服务(lineSearch)
     * 2. 根据返回结果解析，输出解析结果(lineSearch_Callback)
     * 3. 在地图上绘制公交线路()
     */
    var marker,map = new AMap.Map("container", {
        resizeEnable: true,
        center: [116.397428, 39.90923],//地图中心点
        zoom: 13 //地图显示的缩放级别
    });
    /*公交线路查询*/
    function lineSearch() {
        //实例化公交线路查询类，只取回一条路线
        var linesearch = new AMap.LineSearch({
            pageIndex: 2,
            city: '南京',
            pageSize: 2,
            extensions: 'all'
        });
        //搜索“536”相关公交线路
        linesearch.search('${line}', function(status, result) {
            if (status === 'complete' && result.info === 'OK') {
                lineSearch_Callback(result);
            }  
        });
    }
    
   
    /*公交路线查询服务返回数据解析概况*/
    function lineSearch_Callback(data) {
        var lineArr = data.lineInfo;
        var lineNum = data.lineInfo.length;
		document.getElementById("aa").innerHTML=JSON.stringify(data);
        if (lineNum == 0) {
        } else {
            for (var i = 0; i < lineNum; i++) {
                var pathArr = lineArr[i].path;
                var stops = lineArr[i].via_stops;
                var startPot = stops[0].location;
                var endPot = stops[stops.length - 1].location;				
				//alert("|"+stops[0].name+"|"+stops[0].location);
				//alert(stops[stops.length - 1].name+"|"+stops[stops.length - 1].location);
				//alert( stops[1].name+"|"+stops[1].location);
                if (i == 1) {
                	drawbusLine(startPot, endPot, pathArr);
                	drawStation(stops);
                }
            }
        }
    }
    /*绘制路线*/
    function drawbusLine(startPot, endPot, BusArr) {
        //绘制起点，终点
        new AMap.Marker({
            map: map,
            position: [startPot.lng, startPot.lat], //基点位置
            icon: "http://webapi.amap.com/theme/v1.3/markers/n/start.png",
            zIndex: 10
        });
        new AMap.Marker({
            map: map,
            position: [endPot.lng, endPot.lat], //基点位置
            icon: "http://webapi.amap.com/theme/v1.3/markers/n/end.png",
            zIndex: 10
        });
        //绘制乘车的路线
        busPolyline = new AMap.Polyline({
            map: map,
            path: BusArr,
            strokeColor: "#09f",//线颜色
            strokeOpacity: 0.8,//线透明度
            strokeWeight: 6//线宽
        });
        map.setFitView();
    }
    /*绘制站点*/
    function drawStation(stops){
    	  stops.forEach(function(item, idx) {
    	  			console.info(item);
    	  			addMarkerStation(item["location"].lat,item["location"].lng,item["name"]);
      });
    }
    function addMarkerStation(lat,lng,sName) {
      var  marker = new AMap.Marker({
            icon: "http://developer.amap.com/wp-content/uploads/2014/06/marker.png",
            position: [lng, lat]
        });
        marker.setMap(map);
        // 设置label标签
         marker.setLabel({//label默认蓝框白底左上角显示，样式className为：amap-marker-label
            offset: new AMap.Pixel(-10, -20),//修改label相对于maker的位置
            content: sName
        });
    }
    
    lineSearch();
    //----------------------------------------------画点
    // 实例化点标记
    function addMarker(lat,lng) {
        if (marker) {
            return;
        }
        marker = new AMap.Marker({
            icon: "http://webapi.amap.com/theme/v1.3/markers/n/mark_r.png",
            position: [lng, lat]
        });
        marker.setMap(map);
        // 设置label标签
        marker.setLabel({//label默认蓝框白底左上角显示，样式className为：amap-marker-label
            offset: new AMap.Pixel(20, 20),//修改label相对于maker的位置
            content: "${mac}"
        });
    }
    
    function refresh() {
   		   clear();
    	  $.getJSON("./BusMoveSevlet")
    	    .done(function(data) {
    	    	if(data!=null){
    	    		//alert(data["lat"]+"|"+data["lng"]);
        	    	addMarker(data["lat"],data["lng"]);
    	    	}
    	    });
    	}
     function clear(){
    	if (marker) {
            marker.setMap(null);
            marker = null;
        }
    } 
    $(document).ready(function(){
    	setInterval(refresh, 10000);
    });
    
</script>
</body>
</html>                     

                      

    