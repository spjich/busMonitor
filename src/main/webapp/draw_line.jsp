<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page isELIgnored="false"%>
<!doctype html>
<html>
<head>
    <meta charset="gb2312">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no, width=device-width">
    <title>查看线路路径</title>
    <link rel="stylesheet" href="http://cache.amap.com/lbs/static/main1119.css"/>
    <style>
        .info-tip {
            position: absolute;
            top: 10px;
            right: 10px;
            font-size: 12px;
            background-color: #fff;
            height: 35px;
            text-align: left;
        }
    </style>
    <script src="http://cache.amap.com/lbs/static/es5.min.js"></script>
    <script src="http://webapi.amap.com/maps?v=1.3&key=5d9167e66df52a5615d679474dea454a"></script>
    <script type="text/javascript" src="http://cache.amap.com/lbs/static/addToolbar.js"></script>
    <script type="text/javascript" src="jquery-2.0.3.js"></script>
</head>
<body>
<div id="tip2"></div>
<div id="container"></div>
<div class="button-group">
    <input id="setFitView" class="button" type="button" value="地图自适应显示"/>
</div>
<div class="info-tip">
    <div id="centerCoord"></div>
    <div id="tips"></div>
</div>
<script>
	$.ajaxSetup({  
	    async : false  
	});
	var map = new AMap.Map('container', {
        resizeEnable: true,
        center: [118.82612699785818,31.869282249699065],
		layers: [new AMap.TileLayer.Satellite()],
        zoom: 13
    });    
    map.clearMap();  // 清除地图覆盖物
     var markersO = "";
     $.get("./MonitorSevlet",function(data,status){
    	 markersO=data;
	  });
     var markers = eval(markersO);
    // 添加一些分布不均的点到地图上,地图上添加三个点标记，作为参照
    markers.forEach(function(marker) {
        new AMap.Marker({
            map: map,
            icon: marker.icon,
            position: [marker.position[0], marker.position[1]],
            offset: new AMap.Pixel(-12, -36)
        });
    });
    var center = map.setFitView();
    //var centerText = '当前中心点坐标：' + center.getLng() + ',' + center.getLat();
   // document.getElementById('centerCoord').innerHTML = centerText;
    //document.getElementById('tips').innerHTML = '成功添加三个点标记，其中有两个在当前地图视野外！';

    // 添加事件监听, 使地图自适应显示到合适的范围
    AMap.event.addDomListener(document.getElementById('setFitView'), 'click', function() {
        var newCenter = map.setFitView();
        document.getElementById('centerCoord').innerHTML = '当前中心点坐标：' + newCenter.getCenter();
        document.getElementById('tips').innerHTML = '通过setFitView，地图自适应显示到合适的范围内,点标记已全部显示在视野中！';
    });
</script>
</body>
</html>