<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>公交模拟工具</title>
<script type="text/javascript"
	src="http://webapi.amap.com/maps?v=1.3&key=5d9167e66df52a5615d679474dea454a&plugin=AMap.LineSearch"></script>
<script type="text/javascript"
	src="http://cache.amap.com/lbs/static/addToolbar.js"></script>
<script type="text/javascript" src="jquery-2.0.3.js"></script>
</head>
<body>
	<fieldset>
 	线路名称：
	<input id="line" type="text" value="${line }">
	<button id="click" href="####">设置模拟线路</button>
	<br />		 
	</fieldset>
	<br />
	<br />
	<a id="lineView" href="draw_line.jsp">1.查看线路路径</a>
	<br/>
	<a href="./BusMoveJumpSevlet">2.模拟车辆移动</a>
	<a href="./BusResetSevlet">模拟重置</a>
	<br />
	<script language="javascript">
		/*
		 * 该示例主要流程分为三个步骤
		 * 1. 首先调用公交路线查询服务(lineSearch)
		 * 2. 根据返回结果解析，输出解析结果(lineSearch_Callback)
		 * 3. 在地图上绘制公交线路()
		 */
		var marker, map = new AMap.Map("container", {
			resizeEnable : true,
			center : [ 116.397428, 39.90923 ],//地图中心点
			zoom : 13
		//地图显示的缩放级别
		});
		/*公交线路查询*/
		function lineSearch() {
			var line = $("#line").val();
			//实例化公交线路查询类，只取回一条路线
			var linesearch = new AMap.LineSearch({
				pageIndex : 1,
				city : '南京',
				pageSize : 2,
				extensions : 'all'
			});
			//搜索“536”相关公交线路
			linesearch.search(line, function(status, result) {
				if (status === 'complete' && result.info === 'OK') {
					lineSearch_Callback(line, result);
				} else {
					alert(result);
				}
			});
		}
		//ajax提交
		function submitData(line, lineinfo) {
			if (line == undefined || line == "" || line == null) {
				alert("请填写线路名称，例如：99");
			}
			if (line == undefined || line == "" || line == null) {
				alert("未查到该线路的信息");
			}
			//发送post
			$.post("./LineBdSevlet", {
				"lname" : line,
				"lineinfo" : lineinfo
			}, function(data) {
				if (data ==  0 ) {
					$("#line").val(line);
					alert("设置成功");
				} else {
					alert("设置失败");
				}
			});
		}
		/*公交路线查询服务返回数据解析概况*/
		function lineSearch_Callback(line, data) {
			var lineArr = data.lineInfo;
			var lineNum = data.lineInfo.length;
			//document.getElementById("aa").innerHTML= JSON.stringify(lineArr);
			if (lineNum == 0) {
			} else {
				var pathArr = lineArr[1].path;
				//$("#lineinfo").html(JSON.stringify(pathArr));
				submitData(line, JSON.stringify(pathArr));
			}
		}
		$(function() {
			$("#click").click(function() {
				lineSearch();
			});
		});
	</script>
</body>
</html>